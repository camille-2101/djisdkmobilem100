package net.info420.ritc.takeoff;

import android.app.Application;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import dji.common.error.DJIError;
import dji.common.error.DJISDKError;
import dji.sdk.base.DJIBaseComponent;
import dji.sdk.base.DJIBaseProduct;
import dji.sdk.gimbal.DJIGimbal;
import dji.sdk.products.DJIAircraft;
import dji.sdk.sdkmanager.DJISDKManager;

/**
 * Created by ritc on 2/17/17.
 */

public class DroneApplication extends Application {

    private static final String TAG = DroneApplication.class.getName();

    public static final String FLAG_CONNECTION_NAME = "com_dji_Drone_connection_change";

    private static DJIBaseProduct produit;

    private static DJIGimbal gimbale;

    private Handler handler;




    public static synchronized DJIBaseProduct getProduit() {
        if (produit == null) {
            produit = DJISDKManager.getInstance().getDJIProduct();
        }
        return produit;
    }


    public static synchronized DJIGimbal getGimbal() {
        if (null == gimbale) {
            gimbale = DJISDKManager.getInstance().getDJIProduct().getGimbal();
        }
        return gimbale;
    }

    public static boolean isDroneConnecte() {
        return  getProduit() != null && getProduit() instanceof DJIAircraft;
    }


    public static synchronized DJIAircraft getDrone() {
        if (!isDroneConnecte()) return null;
        return (DJIAircraft) getProduit();
    }


    @Override
    public void onCreate() {
        super.onCreate();

        handler = new Handler(Looper.getMainLooper());


        DJISDKManager.getInstance().initSDKManager(this, callback);
    }

    private DJISDKManager.DJISDKManagerCallback callback = new DJISDKManager.DJISDKManagerCallback() {
        @Override
        public void onGetRegisteredResult(DJIError djiError) {
            if (djiError == DJISDKError.REGISTRATION_SUCCESS) {
                DJISDKManager.getInstance().startConnectionToProduct();
                Handler handler1 = new Handler(Looper.getMainLooper());
                handler1.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Enregistrement: Succès", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                Handler handler1 = new Handler(Looper.getMainLooper());
                handler1.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Enregistrement: échoué", Toast.LENGTH_LONG).show();
                    }
                });
            }
            Log.d(TAG, djiError.toString());
        }

        @Override
        public void onProductChanged(DJIBaseProduct vieuxProduit, DJIBaseProduct nouveauProduit) {
            produit = nouveauProduit;

            if (produit != null) {
                produit.setDJIBaseProductListener(listenerProduit);
            }

            notificationChangementStatut();
        }
    };

    private DJIBaseProduct.DJIBaseProductListener listenerProduit = new DJIBaseProduct.DJIBaseProductListener() {
        @Override
        public void onComponentChange(DJIBaseProduct.DJIComponentKey djiComponentKey, DJIBaseComponent djiBaseComponent, DJIBaseComponent djiBaseComponent1) {
            if(djiBaseComponent1 != null) {
                djiBaseComponent1.setDJIComponentListener(listenerComposant);
            }
            notificationChangementStatut();
        }

        @Override
        public void onProductConnectivityChanged(boolean b) {
            notificationChangementStatut();
        }


    };

    private DJIBaseComponent.DJIComponentListener listenerComposant = new DJIBaseComponent.DJIComponentListener() {
        @Override
        public void onComponentConnectivityChanged(boolean b) {
            notificationChangementStatut();
        }
    };

    private void notificationChangementStatut() {
        handler.removeCallbacks(runnableMiseAJour);
        handler.postDelayed(runnableMiseAJour, 500);
    }

    private Runnable runnableMiseAJour = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(FLAG_CONNECTION_NAME);
            sendBroadcast(intent);
        }
    };

}
