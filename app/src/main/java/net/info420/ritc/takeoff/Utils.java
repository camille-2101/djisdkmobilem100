package net.info420.ritc.takeoff;

import dji.sdk.products.DJIAircraft;

/**
 * Created by ritc on 2017-05-16.
 */

public class Utils {

    public static boolean isMannetteDisponible() {
        return isProductModuleAvailable() && isDrone() &&
                (null != DroneApplication.getDrone().getFlightController());
    }

    public static boolean isProductModuleAvailable() {
        return (null != DroneApplication.getProduit());
    }

    public static boolean isDrone() {
        return DroneApplication.getProduit() instanceof DJIAircraft;
    }

}
