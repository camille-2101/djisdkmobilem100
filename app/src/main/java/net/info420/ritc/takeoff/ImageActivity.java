package net.info420.ritc.takeoff;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.opencv.android.*;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import dji.common.error.DJIError;
import dji.common.gimbal.DJIGimbalAngleRotation;
import dji.common.gimbal.DJIGimbalRotateAngleMode;
import dji.common.gimbal.DJIGimbalRotateDirection;
import dji.common.gimbal.DJIGimbalWorkMode;
import dji.common.product.Model;
import dji.common.util.DJICommonCallbacks;
import dji.sdk.base.DJIBaseProduct;
import dji.sdk.camera.DJICamera;
import dji.sdk.codec.DJICodecManager;

public class ImageActivity extends Activity implements TextureView.SurfaceTextureListener, View.OnClickListener {
    private static final String TAG = MainActivity.class.getName();
    protected DJICamera.CameraReceivedVideoDataCallback mReceivedVideoDataCallBack = null;
    protected TextureView mVideoSurface = null;
    private Button mCaptureBtn, mShootPhotoModeBtn;
    private ToggleButton mRecordBtn;
    private DJICodecManager mCodecManager;
    private ImageView imageView;

    static {
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "OpenCV n'as pas chargé");
        } else {
            Log.d(TAG, "OpenCV a chargé correctement");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        initUI();
        // The callback for receiving the raw H264 video data for camera live view
        mReceivedVideoDataCallBack = new DJICamera.CameraReceivedVideoDataCallback() {
            @Override
            public void onResult(byte[] videoBuffer, int size) {
                if (mCodecManager != null) {
                    // Send the raw H264 video data to codec manager for decoding
                    mCodecManager.sendDataToDecoder(videoBuffer, size);
                } else {
                    Log.e(TAG, "mCodecManager is null");
                }
            }
        };
    }

    protected void onProductChange() {
        initPreviewer();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();
        initPreviewer();
        onProductChange();
        if (mVideoSurface == null) {
            Log.e(TAG, "mVideoSurface is null");
        }
    }


    private void initPreviewer() {
        DJIBaseProduct product = DroneApplication.getProduit();
        if (product == null || !product.isConnected()) {
//            showToast(getString(R.string.disconnected));
        } else {
            if (null != mVideoSurface) {
                mVideoSurface.setSurfaceTextureListener(this);
            }
            if (!product.getModel().equals(Model.UnknownAircraft)) {
                DJICamera camera = product.getCamera();
                if (camera != null) {
                    // Set the callback
                    camera.setDJICameraReceivedVideoDataCallback(mReceivedVideoDataCallBack);
                }
            }
        }
    }

    private void uninitPreviewer() {
        DJICamera camera = DroneApplication.getDrone().getCamera();
        if (camera != null) {
            // Reset the callback
            camera.setDJICameraReceivedVideoDataCallback(null);
        }
    }


    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Log.e(TAG, "onSurfaceTextureAvailable");
        if (mCodecManager == null) {
            mCodecManager = new DJICodecManager(this, surface, width, height);
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        Log.e(TAG, "onSurfaceTextureSizeChanged");
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        Log.e(TAG, "onSurfaceTextureDestroyed");
        if (mCodecManager != null) {
            mCodecManager.cleanSurface();
            mCodecManager = null;
        }
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }

    @Override
    public void onClick(View view) {
        final Timer shoot = new Timer();
        switch (view.getId()) {
            case R.id.btn_capture: {
                baisserCamera();
                DroneApplication.getDrone().getFlightController().takeOff(
//                DroneApplication.getDrone().getFlightController().turnOnMotors(
                        new DJICommonCallbacks.DJICompletionCallback() {
                            @Override
                            public void onResult(DJIError djiError) {

                                TimerTask shootTask = new TimerTask() {
                                    @Override
                                    public void run() {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                getImage();
                                            }
                                        });
                                    }
                                };

                                shoot.schedule(shootTask, 0, 200);
                            }
                        }
                );
                break;
            }
            case R.id.btn_shoot_photo_mode: {
                break;
            }
            default:
                break;
        }
    }

    private void initUI() {
        // init mVideoSurface
        mVideoSurface = (TextureView) findViewById(R.id.video_previewer_surface);
        mCaptureBtn = (Button) findViewById(R.id.btn_capture);
        mRecordBtn = (ToggleButton) findViewById(R.id.btn_record);
        mShootPhotoModeBtn = (Button) findViewById(R.id.btn_shoot_photo_mode);
        imageView = (ImageView) findViewById(R.id.imageLigne);

        if (null != mVideoSurface) {
            mVideoSurface.setSurfaceTextureListener(this);
        }

        mCaptureBtn.setOnClickListener(this);
        mRecordBtn.setOnClickListener(this);
        mShootPhotoModeBtn.setOnClickListener(this);
        mRecordBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
    }

    private void getImage() {
        Bitmap image = mVideoSurface.getBitmap();

        Mat image1 = new Mat();
        Mat dst = new Mat();
        Mat hierarchie = new Mat();

        org.opencv.android.Utils.bitmapToMat(image, image1);

        Imgproc.cvtColor(image1, dst, Imgproc.COLOR_BGR2HSV);

        Scalar bas = new Scalar(30, 100, 0);
        Scalar haut = new Scalar(80, 255, 255);

        Core.inRange(dst, bas, haut, dst);

        List<MatOfPoint> contours = new ArrayList<>();

        //http://answers.opencv.org/question/43700/android-using-drawcontours-to-fill-region/
        Imgproc.findContours(dst, contours, hierarchie, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
            Imgproc.drawContours(image1, contours, contourIdx, new Scalar(255, 0, 0), 10);
        }

        //https://stackoverflow.com/questions/18345969/how-to-get-the-mass-center-of-a-contour-android-opencv
        List<Moments> mu = new ArrayList<>();
        for (int x = 0; x < contours.size(); x++) {
            mu.add(x, Imgproc.moments(contours.get(x), false));
            Moments p = mu.get(x);
            int xx = (int) (p.get_m10() / p.get_m00());
            int y = (int) (p.get_m01() / p.get_m00());
            Imgproc.circle(image1, new Point(xx, y), 4, new Scalar(255,49,0,255), 10);
        }

        Toast.makeText(this, contours.size() + "" , Toast.LENGTH_SHORT).show();

        Utils.matToBitmap(image1, image);

        imageView.setImageBitmap(image);

        Utils.matToBitmap(image1, image);

        imageView.setImageBitmap(image);
    }

    public void baisserCamera() {

        DJIGimbalAngleRotation cRoll = new DJIGimbalAngleRotation(true, -90, DJIGimbalRotateDirection.Clockwise);

        DJIGimbalAngleRotation cPitch = new DJIGimbalAngleRotation(true, 0, DJIGimbalRotateDirection.Clockwise);

        DJIGimbalAngleRotation cYaw = new DJIGimbalAngleRotation(true, 0, DJIGimbalRotateDirection.Clockwise);

        DroneApplication.getGimbal().setGimbalWorkMode(DJIGimbalWorkMode.FreeMode, new DJICommonCallbacks.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {

                    }
                }

        );

        DroneApplication.getGimbal().rotateGimbalByAngle(DJIGimbalRotateAngleMode.RelativeAngle, cRoll, cPitch, cYaw, new DJICommonCallbacks.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {

                    }
                }


        );
    }
}
