package net.info420.ritc.takeoff;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import dji.common.error.DJIError;
import dji.common.flightcontroller.DJIVirtualStickFlightControlData;
import dji.common.flightcontroller.DJIVirtualStickFlightCoordinateSystem;
import dji.common.flightcontroller.DJIVirtualStickRollPitchControlMode;
import dji.common.flightcontroller.DJIVirtualStickVerticalControlMode;
import dji.common.flightcontroller.DJIVirtualStickYawControlMode;
import dji.common.util.DJICommonCallbacks;
import dji.sdk.base.DJIBaseProduct;


// CRÉÉ PAR CAMILLE RITCHIE-BEAUDIN

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_decollage, btn_atterissage, btn_parcours, btn_changerActivite;
    private static final String TAG = "TakeOff";
    private float pitch, roll, yaw, throttle;


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.boutonDecoller:
                DroneApplication.getDrone().getFlightController().takeOff(
//                DroneApplication.getDrone().getFlightController().turnOnMotors(
                        new DJICommonCallbacks.DJICompletionCallback() {
                            @Override
                            public void onResult(DJIError djiError) {

                            }
                        }
                );
                break;
            case R.id.boutonAtterir:
                DroneApplication.getDrone().getFlightController().autoLanding(
//                DroneApplication.getDrone().getFlightController().turnOffMotors(
                        new DJICommonCallbacks.DJICompletionCallback() {
                            @Override
                            public void onResult(DJIError djiError) {

                            }
                        }
                );
                break;
            case R.id.boutonParcours:
                try {
                    parcours();
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.boutonChangerActivite:
                startActivity(new Intent(this, ImageActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.VIBRATE,
                            android.Manifest.permission.INTERNET, android.Manifest.permission.ACCESS_WIFI_STATE,
                            android.Manifest.permission.WAKE_LOCK, android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.CHANGE_WIFI_STATE, android.Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.SYSTEM_ALERT_WINDOW,
                            android.Manifest.permission.READ_PHONE_STATE,
                    }
                    , 1);

            setContentView(R.layout.activity_main);

            initUI();

            IntentFilter filtre = new IntentFilter();
            filtre.addAction(DroneApplication.FLAG_CONNECTION_NAME);
            registerReceiver(receveur, filtre);

            throttle = 0;
            roll = 0;
            pitch = 0;
            yaw = 0;


            Log.d(TAG, "onCreate: ");

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onPause()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receveur);
    }

    private void initUI() {

        btn_decollage = (Button) findViewById(R.id.boutonDecoller);
        btn_decollage.setOnClickListener(this);
        btn_decollage.setEnabled(false);

        btn_atterissage = (Button) findViewById(R.id.boutonAtterir);
        btn_atterissage.setOnClickListener(this);
        btn_atterissage.setEnabled(false);

        btn_parcours = (Button) findViewById(R.id.boutonParcours);
        btn_parcours.setOnClickListener(this);
        btn_parcours.setEnabled(false);

        btn_changerActivite = (Button) findViewById(R.id.boutonChangerActivite);
        btn_changerActivite.setOnClickListener(this);
    }

    protected BroadcastReceiver receveur = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rafraichirUI();
        }
    };

    private void rafraichirUI() {
        DJIBaseProduct produit = DroneApplication.getProduit();

        if (produit != null && produit.isConnected()) {
            btn_decollage.setEnabled(true);
            btn_atterissage.setEnabled(true);
            btn_parcours.setEnabled(true);

            DroneApplication.getDrone().getFlightController().setRollPitchControlMode
                    (DJIVirtualStickRollPitchControlMode.Velocity);

            DroneApplication.getDrone().getFlightController().setYawControlMode
                    (DJIVirtualStickYawControlMode.AngularVelocity);

            DroneApplication.getDrone().getFlightController().setVerticalControlMode
                    (DJIVirtualStickVerticalControlMode.Velocity);

            DroneApplication.getDrone().getFlightController().
                    enableVirtualStickControlMode(
                            new DJICommonCallbacks.DJICompletionCallback() {
                                @Override
                                public void onResult(DJIError djiError) {

                                }
                            }
                    );

            DroneApplication.getDrone().getFlightController().
                    setHorizontalCoordinateSystem(
                            DJIVirtualStickFlightCoordinateSystem.Body
                    );


            Toast.makeText(this, "Produit: " + produit.getModel().getDisplayName() + " connecté.", Toast.LENGTH_LONG).show();
        } else {
            btn_decollage.setEnabled(false);
            btn_atterissage.setEnabled(false);
            btn_parcours.setEnabled(false);
        }
    }

    public void run() {

        if (Utils.isMannetteDisponible()) {
            DroneApplication.getDrone().getFlightController()
                    .sendVirtualStickFlightControlData(new DJIVirtualStickFlightControlData(
                            pitch, roll, yaw, throttle), new DJICommonCallbacks.DJICompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                        }
                    });
        }
        Log.d(TAG, "run: ");
    }

// ************************* 1e étape : Avancer d'environ 3 mètres ***************************

    private void parcours() {

        DroneApplication.getDrone().getFlightController().takeOff(
//                DroneApplication.getDrone().getFlightController().turnOnMotors(
                new DJICommonCallbacks.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {

                        roll = 0;
                        pitch = 0;
                        yaw = 0;
                        throttle = 0;
                        new CountDownTimer(1000, 100) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                run();
                            }

                            @Override
                            public void onFinish() {

                                roll = 1;
                                pitch = 0;
                                yaw = 0;
                                throttle = 0;
                                new CountDownTimer(3000, 100) {
                                    @Override
                                    public void onTick(long l) {
                                        run();
                                    }

                                    @Override
                                    public void onFinish() {

//******************2e étape: Attendre d'une seconde********************************
                                        roll = 0;
                                        pitch = 0;
                                        yaw = 0;
                                        throttle = 0;
                                        new CountDownTimer(1000, 100) {
                                            @Override
                                            public void onTick(long l) {
                                                run();
                                            }

                                            @Override
                                            public void onFinish() {

// *****************5e étape: Aller de coté de 7 mètre***********************************
                                                roll = 0;
                                                pitch = 1;
                                                yaw = 0;
                                                throttle = 0;

                                                new CountDownTimer(7000, 100) {
                                                    @Override
                                                    public void onTick(long l) {
                                                        run();
                                                    }

                                                    @Override
                                                    public void onFinish() {

//*********************3e étape: Attendre pendant une seconde  *******************************
                                                        roll = 0;
                                                        pitch = 0;
                                                        yaw = 0;
                                                        throttle = 0;
                                                        new CountDownTimer(1000, 100) {
                                                            @Override
                                                            public void onTick(long l) {
                                                                run();
                                                            }

                                                            @Override
                                                            public void onFinish() {

//************************ 4e étape: reculer de trois mètres*********************
                                                                roll = -1;
                                                                pitch = 0;
                                                                yaw = 0;
                                                                throttle = 0;


                                                                new CountDownTimer(3000, 100) {
                                                                    @Override
                                                                    public void onTick(long l) {
                                                                        run();
                                                                    }

                                                                    @Override
                                                                    public void onFinish() {

// ************************5e étape: Attendre une seconde ******************************
                                                                        roll = 0;
                                                                        pitch = 0;
                                                                        yaw = 0;
                                                                        throttle = 0;

                                                                        new CountDownTimer(1000, 100) {
                                                                            @Override
                                                                            public void onTick(long l) {
                                                                                run();
                                                                            }

                                                                            @Override
                                                                            public void onFinish() {

// ********************6e étape: Aller de côté pendant de 4 mètres*********************************
                                                                                roll = 0;
                                                                                pitch = 1;
                                                                                yaw = 0;
                                                                                throttle = 0;

                                                                                new CountDownTimer(1800, 100) {

                                                                                    @Override
                                                                                    public void onTick(long l) {
                                                                                        run();
                                                                                    }

                                                                                    @Override
                                                                                    public void onFinish() {

//*********************7e étape: Attendre 1 seconde*************************************
                                                                                        roll = 0;
                                                                                        pitch = 0;
                                                                                        yaw = 0;
                                                                                        throttle = 0;

                                                                                        new CountDownTimer(1000, 100) {

                                                                                            @Override
                                                                                            public void onTick(long l) {
                                                                                                run();
                                                                                            }

                                                                                            @Override
                                                                                            public void onFinish() {


//***********************8e étape: faire le tour du poteau******************************
                                                                                                roll = 0;
                                                                                                pitch = 1;
                                                                                                yaw = -45;
                                                                                                throttle = 0;

                                                                                                new CountDownTimer(4000, 100) {
                                                                                                    @Override
                                                                                                    public void onTick(long l) {
                                                                                                        run();
                                                                                                    }

                                                                                                    @Override
                                                                                                    public void onFinish() {

                                                                                                        //ATTENDRE UNE SECONDE

                                                                                                        roll = 0;
                                                                                                        pitch = 0;
                                                                                                        yaw = 0;
                                                                                                        throttle = 0;

                                                                                                        new CountDownTimer(1000, 100) {
                                                                                                            @Override
                                                                                                            public void onTick(long millisUntilFinished) {
                                                                                                                run();
                                                                                                            }

                                                                                                            @Override
                                                                                                            public void onFinish() {

                                                                                                                //SE REPLACER

                                                                                                                roll = 0;
                                                                                                                pitch = 0;
                                                                                                                yaw = 45;
                                                                                                                throttle = 0;

                                                                                                                new CountDownTimer(2500, 100) {
                                                                                                                    @Override
                                                                                                                    public void onTick(long millisUntilFinished) {
                                                                                                                        run();
                                                                                                                    }

                                                                                                                    @Override
                                                                                                                    public void onFinish() {
//**********************12e étape: Attendre 1 seconde*************************************
                                                                                                                        roll = 0;
                                                                                                                        pitch = 0;
                                                                                                                        yaw = 0;
                                                                                                                        throttle = 0;

                                                                                                                        new CountDownTimer(1000, 100) {

                                                                                                                            @Override
                                                                                                                            public void onTick(long l) {
                                                                                                                                run();
                                                                                                                            }

                                                                                                                            @Override
                                                                                                                            public void onFinish() {


//*********************13e étape: Avancer et monter de  7m en diagonal*************************************

                                                                                                                                roll = 1;
                                                                                                                                pitch = 0;
                                                                                                                                yaw = 0;
                                                                                                                                throttle = (float) 0.5;

                                                                                                                                new CountDownTimer(4100, 100) {
                                                                                                                                    @Override
                                                                                                                                    public void onTick(long l) {
                                                                                                                                        run();
                                                                                                                                    }

                                                                                                                                    @Override
                                                                                                                                    public void onFinish() {

//*********************15e étape : descendre et continuer de 7 mètres************************************
                                                                                                                                        roll = 1;
                                                                                                                                        pitch = 0;
                                                                                                                                        yaw = 0;
                                                                                                                                        throttle = (float) -0.6;

                                                                                                                                        new CountDownTimer(3400, 100) {
                                                                                                                                            @Override
                                                                                                                                            public void onTick(long l) {
                                                                                                                                                run();
                                                                                                                                            }

                                                                                                                                            @Override
                                                                                                                                            public void onFinish() {

//***************************17e étape: Attendre une seconde*******************************************
                                                                                                                                                roll = 0;
                                                                                                                                                pitch = 0;
                                                                                                                                                yaw = 0;
                                                                                                                                                throttle = 0;

                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                    @Override
                                                                                                                                                    public void onTick(long l) {
                                                                                                                                                        run();
                                                                                                                                                    }

                                                                                                                                                    @Override
                                                                                                                                                    public void onFinish() {

                                                                                                                                                        //SE REPLACER


                                                                                                                                                        roll = 0;
                                                                                                                                                        pitch = 0;
                                                                                                                                                        yaw = 45;
                                                                                                                                                        throttle = 0;

                                                                                                                                                        new CountDownTimer(700, 100) {
                                                                                                                                                            @Override
                                                                                                                                                            public void onTick(long millisUntilFinished) {
                                                                                                                                                                run();
                                                                                                                                                            }

                                                                                                                                                            @Override
                                                                                                                                                            public void onFinish() {


                                                                                                                                                                //ATTENDRE UNE SECONDE

                                                                                                                                                                roll = 0;
                                                                                                                                                                pitch = 0;
                                                                                                                                                                yaw = 0;
                                                                                                                                                                throttle = 0;

                                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                                    @Override
                                                                                                                                                                    public void onTick(long millisUntilFinished) {
                                                                                                                                                                        run();
                                                                                                                                                                    }

                                                                                                                                                                    @Override
                                                                                                                                                                    public void onFinish() {

//**************************18e étape: Tourner autour du poteau*********************************
                                                                                                                                                                        roll = 0;
                                                                                                                                                                        pitch = -1;
                                                                                                                                                                        yaw = 45;
                                                                                                                                                                        throttle = 0;


                                                                                                                                                                        new CountDownTimer(4600, 100) {
                                                                                                                                                                            @Override
                                                                                                                                                                            public void onTick(long l) {
                                                                                                                                                                                run();
                                                                                                                                                                            }

                                                                                                                                                                            @Override
                                                                                                                                                                            public void onFinish() {

//*****************************19e étape: Attendre une seconde**********************************
                                                                                                                                                                                roll = 0;
                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                yaw = 0;
                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                                                    @Override
                                                                                                                                                                                    public void onTick(long l) {
                                                                                                                                                                                        run();
                                                                                                                                                                                    }

                                                                                                                                                                                    @Override
                                                                                                                                                                                    public void onFinish() {

                                                                                                                                                                                        //SE REPLACER

                                                                                                                                                                                        roll = 0;
                                                                                                                                                                                        pitch = 0;
                                                                                                                                                                                        yaw = -45;
                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                        new CountDownTimer(1000, 100) {
                                                                                                                                                                                            @Override
                                                                                                                                                                                            public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                run();
                                                                                                                                                                                            }

                                                                                                                                                                                            @Override
                                                                                                                                                                                            public void onFinish() {

                                                                                                                                                                                                //ATTENDRE UNE SECONDE

                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                                yaw = 0;
                                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                                                                    @Override
                                                                                                                                                                                                    public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                        run();
                                                                                                                                                                                                    }

                                                                                                                                                                                                    @Override
                                                                                                                                                                                                    public void onFinish() {
//*****************************Avancer de 11m ************************************************
                                                                                                                                                                                                        roll = 1;
                                                                                                                                                                                                        pitch = 0;
                                                                                                                                                                                                        yaw = 0;
                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                        new CountDownTimer(6800, 100) {
                                                                                                                                                                                                            @Override
                                                                                                                                                                                                            public void onTick(long l) {
                                                                                                                                                                                                                run();
                                                                                                                                                                                                            }

                                                                                                                                                                                                            @Override
                                                                                                                                                                                                            public void onFinish() {

                                                                                                                                                                                                                //ATTENDRE UNE SECONDE
                                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                                                yaw = 0;
                                                                                                                                                                                                                throttle = 0;


                                                                                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                    public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                                        run();
                                                                                                                                                                                                                    }

                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                    public void onFinish() {

                                                                                                                                                                                                                        //SE REPLACER

                                                                                                                                                                                                                        roll = 0;
                                                                                                                                                                                                                        pitch = 0;
                                                                                                                                                                                                                        yaw = -45;
                                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                                        new CountDownTimer(1700, 100) {
                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                            public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                                                run();
                                                                                                                                                                                                                            }

                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                            public void onFinish() {


                                                                                                                                                                                                                                //Faire le tour du poteau


                                                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                                                pitch = 1;
                                                                                                                                                                                                                                yaw = -45;
                                                                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                                                                new CountDownTimer(4300, 100) {
                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                    public void onTick(long l) {
                                                                                                                                                                                                                                        run();
                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                    public void onFinish() {

                                                                                                                                                                                                                                        //ATTENDRE UNE SECONDE


                                                                                                                                                                                                                                        roll = 0;
                                                                                                                                                                                                                                        pitch = 0;
                                                                                                                                                                                                                                        yaw = 0;
                                                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                                                        new CountDownTimer(1000, 100) {
                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                            public void onTick(long l) {
                                                                                                                                                                                                                                                run();
                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                            public void onFinish() {

                                                                                                                                                                                                                                                //SE REPLACER
                                                                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                                                                                yaw = 45;
                                                                                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                                                                                new CountDownTimer(1500, 100) {
                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                    public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                                                                        run();
                                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                    public void onFinish() {

                                                                                                                                                                                                                                                        //AVANCER DE 6,5m


                                                                                                                                                                                                                                                        roll = 1;
                                                                                                                                                                                                                                                        pitch = 0;
                                                                                                                                                                                                                                                        yaw = 0;
                                                                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                                                                        new CountDownTimer(5000, 100) {
                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                            public void onTick(long l) {
                                                                                                                                                                                                                                                                run();
                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                            public void onFinish() {

                                                                                                                                                                                                                                                                //ATTENDRE UNE SECONDE

                                                                                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                                                                                                yaw = 0;
                                                                                                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                    public void onTick(long l) {
                                                                                                                                                                                                                                                                        run();
                                                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                    public void onFinish() {

                                                                                                                                                                                                                                                                        //SE REPLACER
                                                                                                                                                                                                                                                                        roll = 0;
                                                                                                                                                                                                                                                                        pitch = 0;
                                                                                                                                                                                                                                                                        yaw = 45;
                                                                                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                                                                                        new CountDownTimer(3000, 100) {
                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                            public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                                                                                                run();
                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                            public void onFinish() {

                                                                                                                                                                                                                                                                                //ATTENDRE UNE SECONDE
                                                                                                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                                                                                                                yaw = 0;
                                                                                                                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                    public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                                                                                                        run();
                                                                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                    public void onFinish() {

                                                                                                                                                                                                                                                                                        //FAIRE LE TOUR  COMPLET DU POTEAU


                                                                                                                                                                                                                                                                                        roll = 0;
                                                                                                                                                                                                                                                                                        pitch = -1;
                                                                                                                                                                                                                                                                                        yaw = 45;
                                                                                                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                                                                                                        new CountDownTimer(4900, 100) {
                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                            public void onTick(long l) {
                                                                                                                                                                                                                                                                                                run();
                                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                            public void onFinish() {

                                                                                                                                                                                                                                                                                                //ATTENDRE UNE SECONDE


                                                                                                                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                                                                                                                                yaw = 0;
                                                                                                                                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                    public void onTick(long l) {
                                                                                                                                                                                                                                                                                                        run();
                                                                                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                    public void onFinish() {

                                                                                                                                                                                                                                                                                                        //SE REPLACER
                                                                                                                                                                                                                                                                                                        roll = 0;
                                                                                                                                                                                                                                                                                                        pitch = 0;
                                                                                                                                                                                                                                                                                                        yaw = -45;
                                                                                                                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                                                                                                                        new CountDownTimer(1200, 100) {
                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                            public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                                                                                                                                run();
                                                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                            public void onFinish() {

                                                                                                                                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                                                                                                                                                yaw = 0;
                                                                                                                                                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                                    public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                                                                                                                                        run();
                                                                                                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                                    public void onFinish() {

                                                                                                                                                                                                                                                                                                                        //AVANCER DE 5,5m

                                                                                                                                                                                                                                                                                                                        roll = 1;
                                                                                                                                                                                                                                                                                                                        pitch = 0;
                                                                                                                                                                                                                                                                                                                        yaw = 0;
                                                                                                                                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                                                                                                                                        new CountDownTimer(4000, 100) {
                                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                                            public void onTick(long l) {
                                                                                                                                                                                                                                                                                                                                run();
                                                                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                                            public void onFinish() {

                                                                                                                                                                                                                                                                                                                                //ATTENDRE UNE SECONDE


                                                                                                                                                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                                                                                                                                                                yaw = 0;
                                                                                                                                                                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                                                    public void onTick(long l) {
                                                                                                                                                                                                                                                                                                                                        run();
                                                                                                                                                                                                                                                                                                                                    }


                                                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                                                    public void onFinish() {

                                                                                                                                                                                                                                                                                                                                        //SE REPLACER

                                                                                                                                                                                                                                                                                                                                        roll = 0;
                                                                                                                                                                                                                                                                                                                                        pitch = 0;
                                                                                                                                                                                                                                                                                                                                        yaw = -45;
                                                                                                                                                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                                                                                                                                                        new CountDownTimer(900, 100) {
                                                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                                                            public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                                                                                                                                                                run();
                                                                                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                                                            public void onFinish() {

                                                                                                                                                                                                                                                                                                                                                //ATTENDRE UNE SECONDE

                                                                                                                                                                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                                                                                                                                                                                yaw = 0;
                                                                                                                                                                                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                                                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                                                                    public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                                                                                                                                                                        run();
                                                                                                                                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                                                                    public void onFinish() {


                                                                                                                                                                                                                                                                                                                                                        //FAIRE UN 360 degrés sur le poteau


                                                                                                                                                                                                                                                                                                                                                        roll = 0;
                                                                                                                                                                                                                                                                                                                                                        pitch = (float)0.05;
                                                                                                                                                                                                                                                                                                                                                        yaw = -45;
                                                                                                                                                                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                                                                                                                                                                        new CountDownTimer(10000, 100) {
                                                                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                                                                            public void onTick(long l) {
                                                                                                                                                                                                                                                                                                                                                                run();
                                                                                                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                                                                            public void onFinish() {

                                                                                                                                                                                                                                                                                                                                                                //ATTENDRE UNE SECONDE

                                                                                                                                                                                                                                                                                                                                                                roll = 0;
                                                                                                                                                                                                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                                                                                                                                                                                                yaw = 0;
                                                                                                                                                                                                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                                                                                                                                                                                                new CountDownTimer(1000, 100) {
                                                                                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                                                                                    public void onTick(long l) {
                                                                                                                                                                                                                                                                                                                                                                        run();
                                                                                                                                                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                                                                                    public void onFinish() {
                                                                                                                                                                                                                                                                                                                                                                        //SE REPLACER
                                                                                                                                                                                                                                                                                                                                                                        roll = 0;
                                                                                                                                                                                                                                                                                                                                                                        pitch = 0;
                                                                                                                                                                                                                                                                                                                                                                        yaw = 45;
                                                                                                                                                                                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                                                                                                                                                                                        new CountDownTimer(1700, 100) {
                                                                                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                                                                                            public void onTick(long millisUntilFinished) {
                                                                                                                                                                                                                                                                                                                                                                                run();
                                                                                                                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                                                                                            public void onFinish() {

                                                                                                                                                                                                                                                                                                                                                                                //AVANCER JUSQU'À LA FIN

                                                                                                                                                                                                                                                                                                                                                                                roll = 1;
                                                                                                                                                                                                                                                                                                                                                                                pitch = 0;
                                                                                                                                                                                                                                                                                                                                                                                yaw = 0;
                                                                                                                                                                                                                                                                                                                                                                                throttle = 0;

                                                                                                                                                                                                                                                                                                                                                                                new CountDownTimer(5000, 100) {
                                                                                                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                                                                                                    public void onTick(long l) {
                                                                                                                                                                                                                                                                                                                                                                                        run();
                                                                                                                                                                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                                                                                                                                                                    @Override
                                                                                                                                                                                                                                                                                                                                                                                    public void onFinish() {

                                                                                                                                                                                                                                                                                                                                                                                        //ATTENDRE UNE SECONDE


                                                                                                                                                                                                                                                                                                                                                                                        roll = 0;
                                                                                                                                                                                                                                                                                                                                                                                        pitch = 0;
                                                                                                                                                                                                                                                                                                                                                                                        yaw = 0;
                                                                                                                                                                                                                                                                                                                                                                                        throttle = 0;

                                                                                                                                                                                                                                                                                                                                                                                        new CountDownTimer(1000, 100) {
                                                                                                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                                                                                                            public void onTick(long l) {
                                                                                                                                                                                                                                                                                                                                                                                                run();
                                                                                                                                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                                                                                                            public void onFinish() {
                                                                                                                                                                                                                                                                                                                                                                                                DroneApplication.getDrone().getFlightController().autoLanding(
//                DroneApplication.getDrone().getFlightController().turnOffMotors(
                                                                                                                                                                                                                                                                                                                                                                                                        new DJICommonCallbacks.DJICompletionCallback() {
                                                                                                                                                                                                                                                                                                                                                                                                            @Override
                                                                                                                                                                                                                                                                                                                                                                                                            public void onResult(DJIError djiError) {

                                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                                                                                                );
                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                }.start();
                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                }.start();
                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                }.start();
                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                }.start();
                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                }.start();
                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                }.start();
                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                }.start();
                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                }.start();
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                }.start();
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                }.start();
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                }.start();
                                                                                                                                                                                                            }
                                                                                                                                                                                                        }.start();
                                                                                                                                                                                                    }
                                                                                                                                                                                                }.start();
                                                                                                                                                                                            }
                                                                                                                                                                                        }.start();
                                                                                                                                                                                    }
                                                                                                                                                                                }.start();
                                                                                                                                                                            }
                                                                                                                                                                        }.start();
                                                                                                                                                                    }
                                                                                                                                                                }.start();
                                                                                                                                                            }
                                                                                                                                                        }.start();
                                                                                                                                                    }
                                                                                                                                                }.start();
                                                                                                                                            }
                                                                                                                                        }.start();
                                                                                                                                    }
                                                                                                                                }.start();
                                                                                                                            }
                                                                                                                        }.start();
                                                                                                                    }
                                                                                                                }.start();
                                                                                                            }
                                                                                                        }.start();
                                                                                                    }
                                                                                                }.start();
                                                                                            }
                                                                                        }.start();
                                                                                    }
                                                                                }.start();
                                                                            }
                                                                        }.start();
                                                                    }
                                                                }.start();
                                                            }
                                                        }.start();
                                                    }
                                                }.start();
                                            }
                                        }.start();
                                    }
                                }.start();
                            }
                        }.start();
                    }
                });
    }
}